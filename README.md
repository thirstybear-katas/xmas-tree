# Christmas Tree Kata
The Christmas Tree. One of the most well known symbols of the Yuletide season. Originating in Germany and with links 
back to pagan and ancient Roman times, the Christmnas Tree has been around for a while.

So let's write a festive program to display one on the console!

## The Problem
The requirements are easy. Write a program to draw a festive tree of a given height on the screen with ASCII art. So a 
tree of height 2 would look like:

<pre>
 X
XXX
 |
</pre>

A tree with height 3 would be

<pre>
  x
 xxx
xxxxx
  |
</pre>

You get the idea. 

The minimum height is 2. Maximum is ...well, what is the size your console? The program should politely tell the user
that the tree cannot be shown.

## Rules
* Test-drive this out according to the Rules of TDD:
  * Just enough test to fail -> just enough code to pass the failing test -> tidy up/refactor
* Do not test or mock what you do not own (how does this affect your code?)

